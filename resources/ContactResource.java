package com.dwbook.phonebook.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("/contact")
@Produces(MediaType.APPLICATION_JSON)
public class ContactResource {
    //method for obtaining information regarding the stored contact

    @GET
    @Path("/{id}")
    public Response getContact(@PathParam("id") int id) {
        return Response.ok("{contact_id: " + id + ", name:" +
                " \"Dummy Name\", phone: \"+5489753924\"}").build();
    }
    //methods for creating, deleting and updating contacts.
    //Since nothing is appended to our base URI, this method does not need to be annotated with @Path .
    @POST
    public Response createContact(@FormParam("name") String name,
                                  @FormParam("phone") String phone)
    {
        return Response
                .created(null)
                .build();
    }

    @PUT
    @Path("/{id}")
    public Response updateContact(
            @PathParam("id") int id,
            @FormParam("name") String name,
            @FormParam("phone") String phone) {
        return Response
                .ok("{contact_id:"+ id +", name: \""+ name +"\", " +
                        "phone: \""+ phone +"\" }")
                .build();
    }

}